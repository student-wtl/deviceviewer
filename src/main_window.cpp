﻿#include "main_window.h"

#include <atlmisc.h>
#include <algorithm>


LRESULT DV::MainWindow::onCreate( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    SetIcon( LoadIcon( NULL, IDI_INFORMATION ) );

    INITCOMMONCONTROLSEX icex;
    icex.dwSize = sizeof( INITCOMMONCONTROLSEX );
    icex.dwICC = ICC_LISTVIEW_CLASSES;
    InitCommonControlsEx( &icex );

    RECT rectControl = {};

    rectControl = { 20, 20, 280, 300 };
    m_cbTypeDevice.Create( m_hWnd,
                           rectControl,
                           "",
                           WS_CHILD | WS_VISIBLE | WS_VSCROLL | CBS_DROPDOWNLIST | CBS_HASSTRINGS,
                           WS_EX_CLIENTEDGE,
                           reinterpret_cast<HMENU>(EControlrs::eC_cbTypeDevice) );

    rectControl = { 310, 20, 550, 45 };
    m_eSearchDevice.Create( m_hWnd,
                            rectControl,
                            "",
                            WS_CHILD | WS_VISIBLE | ES_LEFT,
                            WS_EX_CLIENTEDGE,
                            reinterpret_cast<HMENU>(EControlrs::eC_eSearchDevice) );

    GetClientRect( &rectControl );
    rectControl.top += 90;
    rectControl.left = 10;
    rectControl.right -= rectControl.left;

    int rrrr = GetLastError();
    m_lvTableDevice.Create( m_hWnd,
                            rectControl,
                            0,
                            WS_CHILD | WS_VISIBLE | LVS_REPORT,
                            WS_EX_CLIENTEDGE | LVS_EX_FULLROWSELECT,
                            reinterpret_cast<HMENU>(EControlrs::eC_lvTableDevice) );

    initColumns();
    initDeviceInfoList();
    initComboBoxItems();

    fillDeviceInfoTable();

    DlgResize_Init();

    compareVersion( "2.22.1.444", "10.1111.4.0" );

    return 0;
}

LRESULT DV::MainWindow::onDestroy( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled )
{
    PostQuitMessage( 0 );
    return 0;
}

LRESULT DV::MainWindow::onTextEditChanged( WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled )
{
    if( !DoDataExchange( DDX_SAVE, static_cast<UINT>(EControlrs::eC_eSearchDevice) ) )
        return LRESULT();

    if( !DoDataExchange( DDX_SAVE, static_cast<UINT>(EControlrs::eC_cbTypeDevice) ) )
        return LRESULT();

    m_lvTableDevice.DeleteAllItems();
    int rowCount = 0;

    if( strcmp( m_strTypeDevice, "ALL" ) == 0 )
        for( auto it = m_listDeviceInfo.begin(); it != m_listDeviceInfo.end(); ++it )
            for( int i = 0; i < it->second.size(); ++i )
                if( it->second[i].name.find( m_strSearchData ) == 0 )
                    insertRow( rowCount++, it->first, it->second[i] );
                else
                    for( auto it = m_listDeviceInfo.begin(); it != m_listDeviceInfo.end(); ++it )
                    {
                        if( it->first.compare( m_strTypeDevice ) != 0 )
                            continue;

                        for( int i = 0; i < it->second.size(); ++i )
                            if( it->second[i].name.find( m_strSearchData ) == 0 )
                                insertRow( rowCount++, it->first, it->second[i] );
                        break;
                    }
    return LRESULT();
}

LRESULT DV::MainWindow::onComboBoxItemChanged( WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled )
{
    if( DoDataExchange( DDX_SAVE, static_cast<UINT>(EControlrs::eC_cbTypeDevice) ) )
    {
        m_lvTableDevice.DeleteAllItems();
        if( m_cbTypeDevice.GetCurSel() == 0 )
        {
            fillDeviceInfoTable( m_strTypeDevice );
        }
        else
        {
            auto it = findDevicesByType( std::string( m_strTypeDevice ) );
            if( it != m_listDeviceInfo.end() )
                insertItemByType( it->first, it->second );
        }
    }
    return LRESULT();
}

LRESULT DV::MainWindow::onTableViewHeaderClicked( int idCtrl, LPNMHDR pNmhdr, BOOL& bHandled )
{
    m_isAscSort = !m_isAscSort;

    NMLISTVIEW* res = reinterpret_cast<NMLISTVIEW*>(pNmhdr);
    m_iHeaderSorted = res->iSubItem;

    m_lvTableDevice.SortItemsEx( []( LPARAM lParam1, LPARAM lParam2, LPARAM lParamSort ) -> int
    {
        DV::MainWindow* pThis = reinterpret_cast<DV::MainWindow*>(lParamSort);
        return  pThis->compareItems( static_cast<int>(lParam1), static_cast<int>(lParam2), 0 );
    }, reinterpret_cast<LPARAM>(this) );

    return LRESULT();
}

void DV::MainWindow::initColumns()
{
    LPCSTR arrHeaderText[] = { "Device Name", "Device Type", "Driver Provider", "Driver Date", "Driver Version" };
    const int arrWidthHeader[] = { 265, 200, 100, 75, 95 };

    LVCOLUMNA lvColumn = {};
    lvColumn.mask = LVCF_WIDTH | LVCF_TEXT;
    lvColumn.cx = 250;
    for( int i = 0; i < 5; ++i )
    {
        lvColumn.cx = arrWidthHeader[i];
        lvColumn.pszText = const_cast<LPSTR>(arrHeaderText[i]);
        if( m_lvTableDevice.InsertColumn( i, &lvColumn ) == -1 )
        {
            break;
        }
    }
}

void DV::MainWindow::initComboBoxItems()
{
    m_cbTypeDevice.AddString( "ALL" );
    for( auto it = m_listDeviceInfo.begin(); it != m_listDeviceInfo.end(); ++it )
    {
        m_cbTypeDevice.AddString( it->first.c_str() );
    }
    m_cbTypeDevice.SetCurSel( 0 );
}

void DV::MainWindow::insertItemByType( const std::string& strTypeDevice, const std::vector<DeviceData>& vecDeviceData )
{
    int rowCounter = m_lvTableDevice.GetItemCount();
    for( int i = 0; i < vecDeviceData.size(); ++i )
    {
        insertRow( rowCounter + i, strTypeDevice, vecDeviceData[i] );
    }
}

void DV::MainWindow::insertRow( int index, const std::string& strTypeDevice, const DeviceData& devData )
{
    LVITEM item = {};
    item.iItem = index;
    if( m_lvTableDevice.InsertItem( &item ) == -1 )
        return;
    item.mask = LVIF_TEXT;
    auto itemSetter = [&item, this]( int iSubItem, const std::string& text ) -> bool
    {
        item.iSubItem = iSubItem;
        item.pszText = const_cast<LPSTR>(&text[0]);
        if( !m_lvTableDevice.SetItem( &item ) )
            return false;
        return true;
    };

    if( !itemSetter( 0, devData.name ) )
        return;
    if( !itemSetter( 1, strTypeDevice ) )
        return;
    if( !itemSetter( 2, devData.driverProvider ) )
        return;
    if( !itemSetter( 3, devData.driverDate ) )
        return;
    if( !itemSetter( 4, devData.driverVersion ) )
        return;
}

void DV::MainWindow::fillDeviceInfoTable( const std::string& strTypeDevice )
{
    if( strTypeDevice.empty() || strTypeDevice.compare( "ALL" ) == 0 )
    {
        for( auto it = m_listDeviceInfo.begin(); it != m_listDeviceInfo.end(); ++it )
        {
            insertItemByType( it->first, it->second );
        }
    }
    else
    {
        auto it = findDevicesByType( m_strTypeDevice );
        if( it != m_listDeviceInfo.end() )
            insertItemByType( it->first, it->second );
    }
}

int DV::MainWindow::compareItems( int iItem1, int iItem2, int iSubItem )
{
    TCHAR buffer1[101] = {}, buffer2[101] = {};
    LVITEM lvItem1 = {}, lvItem2 = {};

    lvItem1.mask = LVIF_TEXT;
    lvItem1.pszText = buffer1;
    lvItem1.cchTextMax = sizeof( buffer1 );
    lvItem1.iItem = iItem1;
    lvItem1.iSubItem = m_iHeaderSorted;

    lvItem2.mask = LVIF_TEXT;
    lvItem2.pszText = buffer2;
    lvItem2.cchTextMax = sizeof( buffer2 );
    lvItem2.iItem = iItem2;
    lvItem2.iSubItem = m_iHeaderSorted;

    m_lvTableDevice.GetItem( &lvItem1 );
    m_lvTableDevice.GetItem( &lvItem2 );

    if( m_iHeaderSorted != 4 )
        return m_isAscSort ? strcmp( lvItem1.pszText, lvItem2.pszText ) : strcmp( lvItem2.pszText, lvItem1.pszText );
    else
        return m_isAscSort ? compareVersion( lvItem1.pszText, lvItem2.pszText) : compareVersion( lvItem2.pszText, lvItem1.pszText );
}

void DV::MainWindow::initDeviceInfoList() //Укоротил как смог
{
    SP_DEVINFO_DATA spDevInfoData = { 0 };
    spDevInfoData.cbSize = sizeof( SP_DEVINFO_DATA );
    HDEVINFO  hDevInfo = 0L;
    short  wIndex = 0;

    hDevInfo = SetupDiGetClassDevs( 0L, 0L, NULL, DIGCF_ALLCLASSES | DIGCF_PRESENT );

    if( hDevInfo == (void*)-1 )
        return;
    do
    {
        if( !SetupDiEnumDeviceInfo( hDevInfo, wIndex++, &spDevInfoData ) )
            break;

        SP_DEVINSTALL_PARAMS spDevInstallParams = {};
        spDevInstallParams.cbSize = sizeof( SP_DEVINSTALL_PARAMS );
        if( !SetupDiGetDeviceInstallParams( hDevInfo, &spDevInfoData, &spDevInstallParams ) )
            continue;
        else
        {
            spDevInstallParams.FlagsEx |= DI_FLAGSEX_INSTALLEDDRIVER;
            if( !SetupDiSetDeviceInstallParams( hDevInfo, &spDevInfoData, &spDevInstallParams ) )
                continue;
        }
        if( !SetupDiBuildDriverInfoList( hDevInfo, &spDevInfoData, SPDIT_COMPATDRIVER ) )
            continue;

        SP_DRVINFO_DATA_A spDriverInfoDataA;
        spDriverInfoDataA.cbSize = sizeof( SP_DRVINFO_DATA_A );

        if( !SetupDiEnumDriverInfoA( hDevInfo, &spDevInfoData, SPDIT_COMPATDRIVER, 0, &spDriverInfoDataA ) )
            continue;

        addNewDeviceInfo( hDevInfo, &spDevInfoData, &spDriverInfoDataA );

        SetupDiDestroyDriverInfoList( hDevInfo, &spDevInfoData, SPDIT_COMPATDRIVER );
    } while( true );

    SetupDiDestroyDeviceInfoList( hDevInfo );
}

std::string DV::MainWindow::getDeviceName( PSP_DEVINFO_DATA spDevInfoData, HDEVINFO hDevInfo )
{
    CHAR strBuffer[MAX_PATH] = {};

    if( SetupDiGetDeviceRegistryProperty( hDevInfo,
                                          spDevInfoData,
                                          SPDRP_FRIENDLYNAME,
                                          0L,
                                          reinterpret_cast<PBYTE>(strBuffer),
                                          MAX_PATH,
                                          0 ) )
    {
        return std::string( strBuffer );
    }
    else if( SetupDiGetDeviceRegistryProperty( hDevInfo,
                                               spDevInfoData,
                                               SPDRP_DEVICEDESC,
                                               0L,
                                               reinterpret_cast<PBYTE>(strBuffer),
                                               MAX_PATH,
                                               0 ) )
    {
        return std::string( strBuffer );
    }
    else
        return std::string( "Unknown Device" );
}

std::string DV::MainWindow::getDeviceType( PSP_DEVINFO_DATA spDevInfoData )
{
    CHAR strBuffer[MAX_PATH] = {};

    DWORD dwRequireSize;
    if( SetupDiGetClassDescription( &spDevInfoData->ClassGuid,
                                    strBuffer,
                                    MAX_PATH,
                                    &dwRequireSize ) )
    {
        return std::string( strBuffer );
    }
    else
        return std::string( "Unknown Device Type" );
}

std::string DV::MainWindow::getDriverVersion( PSP_DRVINFO_DATA_A spDriverInfoDataA )
{
    auto versionSeparator = []( DWORDLONG num ) -> int
    {
        return static_cast<int>(num & 0xffff);
    };

    DWORDLONG version = spDriverInfoDataA->DriverVersion;
    std::string strVersion = std::to_string( versionSeparator( (version >> 48) ) ) + "." +
        std::to_string( versionSeparator( (version >> 32) ) ) + "." +
        std::to_string( versionSeparator( (version >> 16) ) ) + "." +
        std::to_string( versionSeparator( version ) );
    return strVersion;
}

std::string DV::MainWindow::getDriverDate( PSP_DRVINFO_DATA_A spDriverInfoDataA )
{
    std::string buffer( 11, '\0' );
    SYSTEMTIME sysTime = {};
    if( FileTimeToSystemTime( &spDriverInfoDataA->DriverDate, &sysTime ) )
    {
        sprintf_s( &buffer[0], buffer.size(), "%04d/%02d/%02d", sysTime.wYear, sysTime.wMonth, sysTime.wDay );
    }
    return buffer;
}

void DV::MainWindow::addNewDeviceInfo( HDEVINFO hDevInfo, PSP_DEVINFO_DATA spDevInfoData, PSP_DRVINFO_DATA_A spDriverInfoDataA )
{
    DeviceData devData{ getDeviceName( spDevInfoData, hDevInfo ),
                            spDriverInfoDataA->ProviderName,
                            getDriverDate( spDriverInfoDataA ),
                            getDriverVersion( spDriverInfoDataA ) };

    std::string strDeviceType = getDeviceType( spDevInfoData );
    auto it = findDevicesByType( strDeviceType );
    if( it == m_listDeviceInfo.end() )
    {
        std::vector<DeviceData> vecDeviceDate;
        vecDeviceDate.push_back( devData );
        m_listDeviceInfo.push_back( std::make_pair( std::string( strDeviceType ), vecDeviceDate ) );
    }
    else
        it->second.push_back( devData );
}

int DV::MainWindow::compareVersion( const std::string& strVersion1, const std::string& strVersion2 )
{
    int arrPartVersion1[4] = {}, arrPartVersion2[4] = {};

    int posStart1 = 0, posStart2 = 0;
    int posEnd1 = 0, posEnd2 = 0;

    for( int i = 0; i < 4; ++i, posStart1 = ++posEnd1, posStart2 = ++posEnd2 )
    {
        posEnd1 = strVersion1.find( '.', posEnd1 );
        posEnd2 = strVersion2.find( '.', posEnd2 );
        arrPartVersion1[i] = std::atoi( strVersion1.substr( posStart1, posEnd1 - posStart1 ).c_str() );
        arrPartVersion2[i] = std::atoi( strVersion2.substr( posStart2, posEnd2 - posStart2 ).c_str() );
        if( arrPartVersion1[i] == arrPartVersion2[i] )
            continue;
        else if( arrPartVersion1[i] < arrPartVersion2[i] )
        {
            return -1;
        }
        else
        {
            return 1;
        }
    }
    return 0;
}

std::list<std::pair<std::string, std::vector<DV::DeviceData>>>::iterator DV::MainWindow::findDevicesByType( const std::string& strTypeDevice )
{
    return std::find_if( m_listDeviceInfo.begin(), m_listDeviceInfo.end(), [&strTypeDevice, this]( const std::pair<std::string, std::vector<DeviceData>>& elem )
    {
        return elem.first.compare( strTypeDevice ) == 0 ? true : false;
    } );
}
