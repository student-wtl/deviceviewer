﻿#pragma once
#include <atlbase.h>
#include <atlapp.h>

extern CAppModule _Module;

#include <atlwin.h>
#include <atlddx.h>
#include <atlframe.h>
#include <atlstr.h>
#include <atlctrls.h>

#include "SetupAPI.h"
#include "device_data.h"

#include <list>
#include <vector>
#include <string>


namespace DV
{
    class MainWindow :
        public CWindowImpl<MainWindow, CWindow, CFrameWinTraits>,
        public CWinDataExchange<MainWindow>,
        public CDialogResize<MainWindow>
    {
        BEGIN_MSG_MAP( MainWindow )
            MESSAGE_HANDLER( WM_CREATE, onCreate )
            MESSAGE_HANDLER( WM_DESTROY, onDestroy )
            CHAIN_MSG_MAP( CDialogResize<MainWindow> )
            COMMAND_HANDLER( static_cast<UINT>(EControlrs::eC_eSearchDevice), EN_CHANGE, onTextEditChanged )
            COMMAND_HANDLER( static_cast<UINT>(EControlrs::eC_cbTypeDevice), CBN_SELCHANGE, onComboBoxItemChanged )
            NOTIFY_HANDLER( static_cast<UINT>(EControlrs::eC_lvTableDevice), LVN_COLUMNCLICK, onTableViewHeaderClicked )
        END_MSG_MAP()
        
        BEGIN_DDX_MAP( MainWindow )
            DDX_TEXT_LEN( static_cast<UINT>(EControlrs::eC_eSearchDevice), m_strSearchData, 150 )
            DDX_TEXT_LEN( static_cast<UINT>(EControlrs::eC_cbTypeDevice), m_strTypeDevice, 150 )
        END_DDX_MAP()

        BEGIN_DLGRESIZE_MAP( MainWindow )
            DLGRESIZE_CONTROL( static_cast<UINT>(EControlrs::eC_lvTableDevice), DLSZ_SIZE_X | DLSZ_SIZE_Y )
        END_DLGRESIZE_MAP()
        
    private:
        enum class EControlrs : UINT8
        {
            eC_lvTableDevice = 1,
            eC_eSearchDevice,
            eC_cbTypeDevice
        };
        
        CListViewCtrl m_lvTableDevice;
        CEdit m_eSearchDevice;
        CComboBox m_cbTypeDevice;
        std::list<std::pair<std::string, std::vector<DeviceData>>> m_listDeviceInfo;
        TCHAR m_strSearchData[151] = {};
        TCHAR m_strTypeDevice[151] = {};
        int m_iHeaderSorted = 0;
        bool m_isAscSort = false;

        LRESULT onDestroy( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );
        LRESULT onCreate( UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled );

        LRESULT onTextEditChanged( WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled );
        LRESULT onComboBoxItemChanged( WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled );
        LRESULT onTableViewHeaderClicked( int idCtrl, LPNMHDR pnmh, BOOL& bHandled );

        std::list<std::pair<std::string, std::vector<DeviceData>>>::iterator findDevicesByType( const std::string& strTypeDevice );

        void initColumns();
        void initComboBoxItems();
        void fillDeviceInfoTable(const std::string& strTypeDevice = "ALL" );

        int compareItems( int iItem1, int iItem2, int iSubItem );
        void initDeviceInfoList();
        void insertItemByType( const std::string& strTypeDevice, const std::vector<DeviceData>& vecDeviceData );
        void insertRow( int index , const std::string& strTypeDevice, const DeviceData& devData );
        std::string getDeviceName( PSP_DEVINFO_DATA spDevInfoData, HDEVINFO hDevInfo );
        std::string getDeviceType( PSP_DEVINFO_DATA spDevInfoData );
        std::string getDriverVersion( PSP_DRVINFO_DATA_A spDriverInfoDataA );
        std::string getDriverDate( PSP_DRVINFO_DATA_A spDriverInfoDataA );
        void addNewDeviceInfo( HDEVINFO hDevInfo, PSP_DEVINFO_DATA spDevInfoData, PSP_DRVINFO_DATA_A spDriverInfoDataA );
        int compareVersion( const std::string& strVersion1, const std::string& strVersion2 );
    };
}
