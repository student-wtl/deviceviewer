﻿#include "main_window.h"

CAppModule _Module;

int WINAPI WinMain( HINSTANCE hInstance, HINSTANCE, LPSTR, int )
{
    _Module.Init( 0, hInstance, 0 );

    const UINT16 width = 800;
    const UINT16 height = 600;

    RECT rectWindow = {};
    rectWindow.left = GetSystemMetrics( SM_CXSCREEN ) / 2 - width / 2;
    rectWindow.top = GetSystemMetrics( SM_CYSCREEN ) / 2 - height / 2;
    rectWindow.right = rectWindow.left + width;
    rectWindow.bottom = rectWindow.top + height;

    DV::MainWindow window;
    window.Create( NULL, rectWindow, "Device Viewer" );
    window.ShowWindow( SW_SHOW );

    CMessageLoop loop;
    int res = loop.Run();

    _Module.Term();

    return res;
}