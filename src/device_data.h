#pragma once
#include <string>

namespace DV
{
    struct DeviceData
    {
        DeviceData() = default;
        DeviceData( std::string name, std::string driverProvider, std::string driverDate, std::string driverVersion)
        {
            this->name = name;
            this->driverProvider = driverProvider;
            this->driverDate = driverDate;
            this->driverVersion = driverVersion;
        }
        std::string name;
        std::string driverProvider;
        std::string driverDate;
        std::string driverVersion;
    };
}